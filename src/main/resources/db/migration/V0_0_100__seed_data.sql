CREATE EXTENSION pgcrypto;

INSERT INTO courses values
(gen_random_uuid(), 'MATH', 'MAIN'),
(gen_random_uuid(), 'SCIENCE', 'MAIN'),
(gen_random_uuid(), 'FINGERPAINT', 'SECONDARY');

INSERT INTO groups values
(gen_random_uuid(), '2A'),
(gen_random_uuid(), '2B'),
(gen_random_uuid(), '10A'),
(gen_random_uuid(), '10B'),
(gen_random_uuid(), '10C');

INSERT INTO users values
(gen_random_uuid(), 'John Doe', 8, 'STUDENT'),
(gen_random_uuid(), 'Sally Doe', 8, 'STUDENT'),
(gen_random_uuid(), 'Billy Doe', 16, 'STUDENT'),
(gen_random_uuid(), 'Sarah Doe', 16, 'STUDENT'),
(gen_random_uuid(), 'Miya Doe', 35, 'TEACHER'),
(gen_random_uuid(), 'Robert Doe', 38, 'TEACHER');

INSERT INTO courses_users (course_id, user_id) values
-- MATH
((SELECT id FROM courses WHERE name = 'MATH' AND type = 'MAIN'), (SELECT id FROM users WHERE name = 'John Doe' AND type = 'STUDENT')),
((SELECT id FROM courses WHERE name = 'MATH' AND type = 'MAIN'), (SELECT id FROM users WHERE name = 'Sally Doe' AND type = 'STUDENT')),
((SELECT id FROM courses WHERE name = 'MATH' AND type = 'MAIN'), (SELECT id FROM users WHERE name = 'Billy Doe' AND type = 'STUDENT')),
((SELECT id FROM courses WHERE name = 'MATH' AND type = 'MAIN'), (SELECT id FROM users WHERE name = 'Sarah Doe' AND type = 'STUDENT')),
((SELECT id FROM courses WHERE name = 'MATH' AND type = 'MAIN'), (SELECT id FROM users WHERE name = 'Miya Doe' AND type = 'TEACHER')),
((SELECT id FROM courses WHERE name = 'MATH' AND type = 'MAIN'), (SELECT id FROM users WHERE name = 'Robert Doe' AND type = 'TEACHER')),
-- SCIENCE
((SELECT id FROM courses WHERE name = 'SCIENCE' AND type = 'MAIN'), (SELECT id FROM users WHERE name = 'Billy Doe' AND type = 'STUDENT')),
((SELECT id FROM courses WHERE name = 'SCIENCE' AND type = 'MAIN'), (SELECT id FROM users WHERE name = 'Sarah Doe' AND type = 'STUDENT')),
((SELECT id FROM courses WHERE name = 'SCIENCE' AND type = 'MAIN'), (SELECT id FROM users WHERE name = 'Miya Doe' AND type = 'TEACHER')),
((SELECT id FROM courses WHERE name = 'SCIENCE' AND type = 'MAIN'), (SELECT id FROM users WHERE name = 'Robert Doe' AND type = 'TEACHER')),
-- FINGERPAINT
((SELECT id FROM courses WHERE name = 'FINGERPAINT' AND type = 'SECONDARY'), (SELECT id FROM users WHERE name = 'John Doe' AND type = 'STUDENT')),
((SELECT id FROM courses WHERE name = 'FINGERPAINT' AND type = 'SECONDARY'), (SELECT id FROM users WHERE name = 'Sally Doe' AND type = 'STUDENT')),
((SELECT id FROM courses WHERE name = 'FINGERPAINT' AND type = 'SECONDARY'), (SELECT id FROM users WHERE name = 'Robert Doe' AND type = 'TEACHER'));


INSERT INTO groups_users (group_id, user_id) values
((SELECT id FROM groups WHERE name = '2A'), (SELECT id FROM users WHERE name = 'John Doe' AND type = 'STUDENT')),
((SELECT id FROM groups WHERE name = '2B'), (SELECT id FROM users WHERE name = 'Sally Doe' AND type = 'STUDENT')),
((SELECT id FROM groups WHERE name = '10A'), (SELECT id FROM users WHERE name = 'Billy Doe' AND type = 'STUDENT')),
((SELECT id FROM groups WHERE name = '10B'), (SELECT id FROM users WHERE name = 'Sarah Doe' AND type = 'STUDENT')),
((SELECT id FROM groups WHERE name = '2A'), (SELECT id FROM users WHERE name = 'Miya Doe' AND type = 'TEACHER')),
((SELECT id FROM groups WHERE name = '2B'), (SELECT id FROM users WHERE name = 'Miya Doe' AND type = 'TEACHER')),
((SELECT id FROM groups WHERE name = '10A'), (SELECT id FROM users WHERE name = 'Robert Doe' AND type = 'TEACHER')),
((SELECT id FROM groups WHERE name = '10B'), (SELECT id FROM users WHERE name = 'Robert Doe' AND type = 'TEACHER'));



