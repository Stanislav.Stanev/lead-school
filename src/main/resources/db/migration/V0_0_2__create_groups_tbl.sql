CREATE TYPE group_name as ENUM
(
    '2A',
    '2B',
    '10A',
    '10B',
    '10C'
);

CREATE TABLE groups (
    id UUID PRIMARY KEY,
    name group_name NOT NULL UNIQUE
);