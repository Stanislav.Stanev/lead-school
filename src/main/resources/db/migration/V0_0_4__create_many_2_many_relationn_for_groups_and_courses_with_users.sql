CREATE TABLE courses_users (
  course_id UUID,
  user_id UUID,

  PRIMARY KEY (course_id, user_id),
  CONSTRAINT fk_courses FOREIGN KEY(course_id) REFERENCES courses(id),
  CONSTRAINT fk_users FOREIGN KEY(user_id) REFERENCES users(id)
);

CREATE TABLE groups_users (
  group_id UUID,
  user_id UUID,

  PRIMARY KEY (group_id, user_id),
  CONSTRAINT fk_groups FOREIGN KEY(group_id) REFERENCES groups(id),
  CONSTRAINT fk_users FOREIGN KEY(user_id) REFERENCES users(id)
);