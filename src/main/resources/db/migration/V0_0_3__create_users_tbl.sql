CREATE TYPE user_type AS ENUM
(
    'STUDENT',
    'TEACHER'
);

CREATE TABLE users (
    id UUID PRIMARY KEY,
    name VARCHAR NOT NULL,
    age NUMERIC NOT NULL,
    type user_type NOT NULL
);