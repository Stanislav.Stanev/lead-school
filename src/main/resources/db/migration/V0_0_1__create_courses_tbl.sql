CREATE TYPE course_name AS ENUM
(
    'MATH',
    'SCIENCE',
    'FINGERPAINT'
);

CREATE TYPE course_type AS ENUM
(
    'MAIN',
    'SECONDARY'
);

CREATE TABLE courses (
    id UUID PRIMARY KEY,
    name course_name NOT NULL UNIQUE,
    type course_type NOT NULL
);