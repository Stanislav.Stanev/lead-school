package com.lead.school.teacher.service;

import com.lead.school.common.web.GetCountResponse;
import com.lead.school.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeacherService {

    @Autowired
    private UserRepository userRepository;

    public GetCountResponse getCount() {
        return new GetCountResponse(userRepository.getTeacherCount());
    }
}
