package com.lead.school.teacher.web;

import com.lead.school.common.web.GetCountResponse;
import com.lead.school.teacher.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/teachers")
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    @GetMapping("/count")
    public GetCountResponse getCount() {
        return teacherService.getCount();
    }
}
