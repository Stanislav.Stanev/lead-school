package com.lead.school.repository;

import com.lead.school.enums.CourseName;
import com.lead.school.enums.GroupName;
import com.lead.school.enums.UserType;
import com.lead.school.student.service.User;
import com.lead.school.user.web.domain.UserCreate;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static com.lead.school.Tables.*;
import static org.jooq.impl.DSL.and;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    private DSLContext context;

    @Override
    public Integer getStudentCount() {
        return context.fetchCount(
                context.selectFrom(USERS)
                        .where(USERS.TYPE.eq(UserType.STUDENT))
        );
    }

    @Override
    public Integer getTeacherCount() {
        return context.fetchCount(
                context.selectFrom(USERS)
                        .where(USERS.TYPE.eq(UserType.TEACHER))
        );
    }

    @Override
    public List<User> getCourseStudents(CourseName courseName) {
        return context.select()
                .from(USERS)
                .join(COURSES_USERS).on(COURSES_USERS.USER_ID.eq(USERS.ID))
                .join(COURSES).on(COURSES.ID.eq(COURSES_USERS.COURSE_ID))
                .where(COURSES.NAME.eq(courseName)).and(USERS.TYPE.eq(UserType.STUDENT))
                .fetchInto(USERS)
                .into(User.class);
    }

    @Override
    public List<User> getGroupStudents(GroupName groupName) {
        return context.select()
                .from(USERS)
                .join(GROUPS_USERS).on(GROUPS_USERS.USER_ID.eq(USERS.ID))
                .join(GROUPS).on(GROUPS.ID.eq(GROUPS_USERS.GROUP_ID))
                .where(GROUPS.NAME.eq(groupName)).and(USERS.TYPE.eq(UserType.STUDENT))
                .fetchInto(USERS)
                .into(User.class);
    }

    @Override
    public List<User> getTeachersAndStudents(GroupName groupName, CourseName courseName) {
        return context.select()
                .from(USERS)
                .join(GROUPS_USERS).on(GROUPS_USERS.USER_ID.eq(USERS.ID))
                .join(GROUPS).on(GROUPS.ID.eq(GROUPS_USERS.GROUP_ID))
                .join(COURSES_USERS).on(COURSES_USERS.USER_ID.eq(USERS.ID))
                .join(COURSES).on(COURSES.ID.eq(COURSES_USERS.COURSE_ID))
                .where(and(GROUPS.NAME.eq(groupName), (COURSES.NAME.eq(courseName))))
                .fetchInto(USERS)
                .into(User.class);
    }

    @Override
    public List<User> getStudentsByCourseAndAge(CourseName courseName, BigDecimal age) {
        return context.select()
                .from(USERS)
                .join(COURSES_USERS).on(COURSES_USERS.USER_ID.eq(USERS.ID))
                .join(COURSES).on(COURSES.ID.eq(COURSES_USERS.COURSE_ID))
                .where(USERS.AGE.greaterThan(age))
                .and(USERS.TYPE.eq(UserType.STUDENT)).and(COURSES.NAME.eq(courseName))
                .fetchInto(USERS)
                .into(User.class);
    }

    @Override
    public UUID createUser(UserCreate userCreate) {
        Record1<UUID> newRecordId = context.insertInto(USERS,
                        USERS.ID,
                        USERS.NAME,
                        USERS.AGE,
                        USERS.TYPE
                ).values(UUID.randomUUID(),
                        userCreate.getName(),
                        userCreate.getAge(),
                        userCreate.getUserType())
                .returningResult(USERS.ID)
                .fetchOne();

        return newRecordId.getValue(USERS.ID);
    }

    @Override
    public void deleteUser(UUID userId) {
        context.deleteFrom(USERS)
                .where(USERS.ID.eq(userId))
                .execute();
    }
}
