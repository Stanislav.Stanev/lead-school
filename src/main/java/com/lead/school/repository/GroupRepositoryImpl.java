package com.lead.school.repository;

import com.lead.school.enums.GroupName;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

import static com.lead.school.Tables.GROUPS;
import static com.lead.school.Tables.GROUPS_USERS;

@Repository
public class GroupRepositoryImpl implements GroupRepository {

    @Autowired
    private DSLContext context;

    @Override
    public void addUserGroups(UUID userId, List<GroupName> groupNameTypes) {
        groupNameTypes.forEach(groupName -> {
            context.insertInto(GROUPS_USERS,
                            GROUPS_USERS.USER_ID,
                            GROUPS_USERS.GROUP_ID)
                    .values(userId, context.select().from(GROUPS).where(GROUPS.NAME.eq(groupName)).fetchOne(GROUPS.ID))
                    .execute();
        });
    }

    @Override
    public void deleteGroupsForUser(UUID userId) {
        context.deleteFrom(GROUPS_USERS)
                .where(GROUPS_USERS.USER_ID.eq(userId))
                .execute();
    }
}
