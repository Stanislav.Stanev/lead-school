package com.lead.school.repository;

import com.lead.school.enums.GroupName;

import java.util.List;
import java.util.UUID;

public interface GroupRepository {

    void addUserGroups(UUID userId, List<GroupName> groupNameTypes);

    void deleteGroupsForUser(UUID userId);
}
