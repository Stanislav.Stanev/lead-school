package com.lead.school.repository;

import com.lead.school.enums.CourseName;
import com.lead.school.enums.GroupName;
import com.lead.school.student.service.User;
import com.lead.school.user.web.domain.UserCreate;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public interface UserRepository {

    Integer getStudentCount();

    Integer getTeacherCount();

    List<User> getCourseStudents(CourseName courseName);

    List<User> getGroupStudents(GroupName groupName);

    List<User> getTeachersAndStudents(GroupName groupName, CourseName courseName);

    List<User> getStudentsByCourseAndAge(CourseName courseName, BigDecimal age);

    UUID createUser(UserCreate userCreate);

    void deleteUser(UUID userId);
}
