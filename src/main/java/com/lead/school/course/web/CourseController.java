package com.lead.school.course.web;

import com.lead.school.common.web.GetCountResponse;
import com.lead.school.enums.CourseType;
import com.lead.school.course.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/courses")
public class CourseController {

    @Autowired
    private CourseService courseService;

    @GetMapping("/count")
    public GetCountResponse getCount(@RequestParam CourseType courseType) {
        return courseService.getCount(courseType);
    }
}
