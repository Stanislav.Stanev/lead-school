package com.lead.school.course.repository;

import com.lead.school.enums.CourseName;
import com.lead.school.enums.CourseType;

import java.util.List;
import java.util.UUID;

public interface CourseRepository {

    Integer getCount(CourseType courseType);

    void addUserCourses(UUID userId, List<CourseName> courseNameTypes);

    void deleteCoursesForUser(UUID userId);
}
