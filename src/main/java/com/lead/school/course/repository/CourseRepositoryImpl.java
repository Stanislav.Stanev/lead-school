package com.lead.school.course.repository;

import com.lead.school.enums.CourseName;
import com.lead.school.enums.CourseType;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

import static com.lead.school.Tables.COURSES_USERS;
import static com.lead.school.tables.Courses.COURSES;

@Repository
public class CourseRepositoryImpl implements CourseRepository {

    @Autowired
    private DSLContext context;

    @Override
    public Integer getCount(CourseType courseType) {
        return context.fetchCount(
                context.selectFrom(COURSES)
                        .where(COURSES.TYPE.eq(courseType))
        );
    }

    @Override
    public void addUserCourses(UUID userId, List<CourseName> courseNames) {
        courseNames.forEach(courseName -> context.insertInto(COURSES_USERS,
                        COURSES_USERS.USER_ID,
                        COURSES_USERS.COURSE_ID)
                .values(userId, context.select().from(COURSES).where(COURSES.NAME.eq(courseName)).fetchOne(COURSES.ID))
                .execute()
        );
    }

    @Override
    public void deleteCoursesForUser(UUID userId) {
        context.deleteFrom(COURSES_USERS)
                .where(COURSES_USERS.USER_ID.eq(userId))
                .execute();
    }
}
