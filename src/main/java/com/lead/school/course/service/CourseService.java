package com.lead.school.course.service;

import com.lead.school.common.web.GetCountResponse;
import com.lead.school.enums.CourseType;
import com.lead.school.course.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseService {

    @Autowired
    private CourseRepository courseRepository;

    public GetCountResponse getCount(CourseType courseType) {
        return new GetCountResponse(courseRepository.getCount(courseType));
    }
}
