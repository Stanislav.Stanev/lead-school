package com.lead.school.student.web;

import com.lead.school.common.web.GetCountResponse;
import com.lead.school.enums.CourseName;
import com.lead.school.enums.GroupName;
import com.lead.school.student.service.StudentService;
import com.lead.school.student.web.response.GetCourseAndAgeStudentsResponse;
import com.lead.school.student.web.response.GetCourseStudentsResponse;
import com.lead.school.student.web.response.GetGroupStudentsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping("/count")
    public GetCountResponse getCount() {
        return studentService.getCount();
    }

    @GetMapping("/courses/{courseName}")
    public GetCourseStudentsResponse getCourseStudents(@PathVariable CourseName courseName) {
        return studentService.getCourseStudents(courseName);
    }

    @GetMapping("/groups/{groupName}")
    public GetGroupStudentsResponse getGroupStudents(@PathVariable("groupName") GroupName groupName) {
        return studentService.getGroupStudents(groupName);
    }

    @GetMapping("/courses/{courseName}/age/{age}")
    public GetCourseAndAgeStudentsResponse getCourseAndAgeStudents(@PathVariable("courseName") CourseName courseName, @PathVariable BigDecimal age) {
        return studentService.getCourseAndAgeStudents(courseName, age);
    }
}
