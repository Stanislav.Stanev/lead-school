package com.lead.school.student.web.response;

import com.lead.school.student.service.User;

import java.util.List;

public class GetCourseStudentsResponse extends GetStudentsResponse {

    public GetCourseStudentsResponse(List<User> students) {
        super(students);
    }
}
