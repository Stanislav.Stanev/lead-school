package com.lead.school.student.web.response;

import com.lead.school.student.service.User;

import java.util.List;

public abstract class GetStudentsResponse {

    private List<User> students;

    public GetStudentsResponse(List<User> students) {
        this.students = students;
    }

    public List<User> getStudents() {
        return students;
    }
}
