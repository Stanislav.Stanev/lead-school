package com.lead.school.student.web.response;

import com.lead.school.student.service.User;

import java.util.List;

public class GetGroupStudentsResponse extends GetStudentsResponse {

    public GetGroupStudentsResponse(List<User> students) {
        super(students);
    }
}
