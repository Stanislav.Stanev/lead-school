package com.lead.school.student.web.response;

import com.lead.school.student.service.User;

import java.util.List;

public class GetCourseAndAgeStudentsResponse extends GetCourseStudentsResponse {

    public GetCourseAndAgeStudentsResponse(List<User> students) {
        super(students);
    }
}
