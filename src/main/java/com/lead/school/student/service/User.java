package com.lead.school.student.service;

import com.lead.school.enums.UserType;

import java.util.UUID;

public class User {

    private UUID id;
    private String name;
    private Integer age;
    private UserType type;

    public User(UUID id, String name, Integer age, UserType type) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.type = type;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public UserType getType() {
        return type;
    }
}
