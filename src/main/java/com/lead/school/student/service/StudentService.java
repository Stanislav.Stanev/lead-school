package com.lead.school.student.service;

import com.lead.school.common.web.GetCountResponse;
import com.lead.school.enums.CourseName;
import com.lead.school.enums.GroupName;
import com.lead.school.repository.UserRepository;
import com.lead.school.student.web.response.GetCourseAndAgeStudentsResponse;
import com.lead.school.student.web.response.GetCourseStudentsResponse;
import com.lead.school.student.web.response.GetGroupStudentsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class StudentService {

    @Autowired
    private UserRepository userRepository;

    public GetCountResponse getCount() {
        return new GetCountResponse(userRepository.getStudentCount());
    }

    public GetCourseStudentsResponse getCourseStudents(CourseName courseName) {
        List<User> users = userRepository.getCourseStudents(courseName);

        return new GetCourseStudentsResponse(users);
    }

    public GetGroupStudentsResponse getGroupStudents(GroupName groupName) {
        List<User> users = userRepository.getGroupStudents(groupName);

        return new GetGroupStudentsResponse(users);
    }

    public GetCourseAndAgeStudentsResponse getCourseAndAgeStudents(CourseName courseName, BigDecimal age) {
        List<User> students = userRepository.getStudentsByCourseAndAge(courseName, age);

        return new GetCourseAndAgeStudentsResponse(students);
    }
}
