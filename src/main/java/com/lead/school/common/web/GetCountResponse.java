package com.lead.school.common.web;

public class GetCountResponse {

    private Integer count;

    public GetCountResponse(Integer count) {
        this.count = count;
    }

    public Integer getCount() {
        return count;
    }
}
