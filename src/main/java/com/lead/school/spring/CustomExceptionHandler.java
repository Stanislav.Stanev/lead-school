package com.lead.school.spring;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@ControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionHandlerResponse handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
        return new ExceptionHandlerResponse(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionHandlerResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        return new ExceptionHandlerResponse(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionHandlerResponse handleIllegalArgumentException(IllegalArgumentException e) {
        return new ExceptionHandlerResponse(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    private class ExceptionHandlerResponse {

        private HttpStatus status;
        private String errorMessage;

        public ExceptionHandlerResponse(HttpStatus status, String errorMessage) {
            this.status = status;
            this.errorMessage = errorMessage;
        }

        public HttpStatus getStatus() {
            return status;
        }

        public String getErrorMessage() {
            return errorMessage;
        }
    }
}
