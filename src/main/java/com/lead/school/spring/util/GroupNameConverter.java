package com.lead.school.spring.util;

import com.lead.school.enums.GroupName;
import org.springframework.core.convert.converter.Converter;

public class GroupNameConverter implements Converter<String, GroupName> {

    @Override
    public GroupName convert(String source) {
        return GroupName.valueOf("_" + source.toUpperCase());
    }
}
