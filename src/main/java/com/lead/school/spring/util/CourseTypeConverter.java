package com.lead.school.spring.util;

import com.lead.school.enums.CourseType;
import org.springframework.core.convert.converter.Converter;

public class CourseTypeConverter implements Converter<String, CourseType> {

    @Override
    public CourseType convert(String source) {
        return CourseType.valueOf(source.toUpperCase());
    }
}
