package com.lead.school.spring.util;

import com.lead.school.enums.CourseName;
import org.springframework.core.convert.converter.Converter;

public class CourseNameConverter implements Converter<String, CourseName> {

    @Override
    public CourseName convert(String source) {
        return CourseName.valueOf(source.toUpperCase());
    }
}
