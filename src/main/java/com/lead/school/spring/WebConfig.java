package com.lead.school.spring;

import com.lead.school.spring.util.CourseNameConverter;
import com.lead.school.spring.util.CourseTypeConverter;
import com.lead.school.spring.util.GroupNameConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new GroupNameConverter());
        registry.addConverter(new CourseNameConverter());
        registry.addConverter(new CourseTypeConverter());
    }
}