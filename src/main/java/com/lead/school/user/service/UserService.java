package com.lead.school.user.service;

import com.lead.school.course.repository.CourseRepository;
import com.lead.school.enums.CourseName;
import com.lead.school.enums.GroupName;
import com.lead.school.enums.UserType;
import com.lead.school.repository.GroupRepository;
import com.lead.school.repository.UserRepository;
import com.lead.school.student.service.User;
import com.lead.school.user.web.domain.UserCreate;
import com.lead.school.user.web.response.CreateUserResponse;
import com.lead.school.user.web.response.GetTeachersAndStudentsForGroupAndCourseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private GroupRepository groupRepository;

    public GetTeachersAndStudentsForGroupAndCourseResponse getAllTeachersAndStudents(GroupName groupName, CourseName courseName) {
        List<User> allUsers = userRepository.getTeachersAndStudents(groupName, courseName);

        return GetTeachersAndStudentsForGroupAndCourseResponse.Builder.aResponse()
                .withTeachers(allUsers.stream().filter(u -> u.getType().equals(UserType.TEACHER)).collect(Collectors.toList()))
                .withStudents(allUsers.stream().filter(u -> u.getType().equals(UserType.STUDENT)).collect(Collectors.toList()))
                .build();
    }

    @Transactional
    public CreateUserResponse createUser(UserCreate userCreate) {
        UUID userId = userRepository.createUser(userCreate);

        if (userCreate.getCourses().size() > 0) {
            courseRepository.addUserCourses(userId, userCreate.getCourseNameTypes());
        }

        if (userCreate.getGroups().size() > 0) {
            groupRepository.addUserGroups(userId, userCreate.getGroupNameTypes());
        }

        return new CreateUserResponse(userId);
    }

    @Transactional
    public void deleteUser(UUID userId) {
        courseRepository.deleteCoursesForUser(userId);
        groupRepository.deleteGroupsForUser(userId);
        userRepository.deleteUser(userId);
    }
}
