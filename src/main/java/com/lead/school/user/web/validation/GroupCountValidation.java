package com.lead.school.user.web.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target({ElementType.TYPE})
@Constraint(validatedBy = GroupCountValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface GroupCountValidation {

    String message() default "Invalid group count";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
