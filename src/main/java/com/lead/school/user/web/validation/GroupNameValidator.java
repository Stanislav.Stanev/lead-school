package com.lead.school.user.web.validation;

import com.lead.school.enums.GroupName;
import com.lead.school.user.web.domain.UserCreate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

public class GroupNameValidator implements ConstraintValidator<GroupNameValidation, UserCreate> {

    @Override
    public void initialize(GroupNameValidation constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(UserCreate value, ConstraintValidatorContext context) {
        return value.getGroups().stream().allMatch(g -> {
          return Arrays.stream(GroupName.values()).anyMatch(groupName -> groupName.getLiteral().equals(g.toUpperCase()));
        });
    }
}
