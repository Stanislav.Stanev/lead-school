package com.lead.school.user.web.validation;

import com.lead.school.enums.UserType;
import com.lead.school.user.web.domain.UserCreate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class GroupCountValidator implements ConstraintValidator<GroupCountValidation, UserCreate> {

    @Override
    public void initialize(GroupCountValidation constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(UserCreate value, ConstraintValidatorContext context) {
        return value.getUserType().equals(UserType.TEACHER) ||
                (value.getUserType().equals(UserType.STUDENT) && value.getGroups().size() == 1);
    }
}
