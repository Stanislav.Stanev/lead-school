package com.lead.school.user.web.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target({ElementType.TYPE})
@Constraint(validatedBy = CourseNameValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface CourseNameValidation {

    String message() default "Invalid course name";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
