package com.lead.school.user.web.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lead.school.enums.CourseName;
import com.lead.school.enums.GroupName;
import com.lead.school.enums.UserType;
import com.lead.school.user.web.validation.CourseNameValidation;
import com.lead.school.user.web.validation.GroupCountValidation;
import com.lead.school.user.web.validation.GroupNameValidation;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@GroupNameValidation
@GroupCountValidation
@CourseNameValidation
public class UserCreate {

    @NotEmpty
    private String name;

    @NotNull
    private BigDecimal age;

    @NotNull
    private UserType userType;

    private List<String> groups;

    private List<String> courses;

    public UserCreate() {
    }

    public UserCreate(String name, BigDecimal age, UserType userType, List<String> groups, List<String> courses) {
        this.name = name;
        this.age = age;
        this.userType = userType;
        this.groups = groups;
        this.courses = courses;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getAge() {
        return age;
    }

    public UserType getUserType() {
        return userType;
    }

    public List<String> getGroups() {
        return groups;
    }

    public List<String> getCourses() {
        return courses;
    }

    @JsonIgnore
    public List<GroupName> getGroupNameTypes() {
        return getGroups().stream().map(g -> GroupName.valueOf("_" + g.toUpperCase())).collect(Collectors.toList());
    }

    @JsonIgnore
    public List<CourseName> getCourseNameTypes() {
        return getCourses().stream().map(c -> CourseName.valueOf(c.toUpperCase())).collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserCreate that = (UserCreate) o;
        return Objects.equals(name, that.name) && Objects.equals(age, that.age) && userType == that.userType && Objects.equals(groups, that.groups) && Objects.equals(courses, that.courses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, userType, groups, courses);
    }
}
