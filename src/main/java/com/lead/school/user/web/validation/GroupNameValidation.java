package com.lead.school.user.web.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target({ElementType.TYPE})
@Constraint(validatedBy = GroupNameValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface GroupNameValidation {

    String message() default "Invalid group name";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}


