package com.lead.school.user.web.response;

import com.lead.school.student.service.User;

import java.util.List;

public class GetTeachersAndStudentsForGroupAndCourseResponse {

    private List<User> teachers;
    private List<User> students;

    public GetTeachersAndStudentsForGroupAndCourseResponse(List<User> teachers, List<User> students) {
        this.teachers = teachers;
        this.students = students;
    }

    public List<User> getTeachers() {
        return teachers;
    }

    public List<User> getStudents() {
        return students;
    }

    public static final class Builder {
        private List<User> teachers;
        private List<User> students;

        private Builder() {
        }

        public static Builder aResponse() {
            return new Builder();
        }

        public Builder withTeachers(List<User> teachers) {
            this.teachers = teachers;
            return this;
        }

        public Builder withStudents(List<User> students) {
            this.students = students;
            return this;
        }

        public GetTeachersAndStudentsForGroupAndCourseResponse build() {
            return new GetTeachersAndStudentsForGroupAndCourseResponse(teachers, students);
        }
    }
}
