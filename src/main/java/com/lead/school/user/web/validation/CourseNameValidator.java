package com.lead.school.user.web.validation;

import com.lead.school.enums.CourseName;
import com.lead.school.user.web.domain.UserCreate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

public class CourseNameValidator implements ConstraintValidator<CourseNameValidation, UserCreate> {

    @Override
    public void initialize(CourseNameValidation constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(UserCreate value, ConstraintValidatorContext context) {
        return value.getCourses().stream().allMatch(c -> {
            return Arrays.stream(CourseName.values()).anyMatch(courseName -> courseName.getLiteral().equals(c.toUpperCase()));
        });
    }
}
