package com.lead.school.user.web;

import com.lead.school.enums.CourseName;
import com.lead.school.enums.GroupName;
import com.lead.school.user.web.domain.UserCreate;
import com.lead.school.user.web.response.CreateUserResponse;
import com.lead.school.user.web.response.GetTeachersAndStudentsForGroupAndCourseResponse;
import com.lead.school.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/groups/{groupName}/courses/{courseName}")
    public GetTeachersAndStudentsForGroupAndCourseResponse getAllTeachersAndStudentsForGroupAndCourse(@PathVariable("groupName") GroupName groupName, @PathVariable("courseName") CourseName courseName) {
        return userService.getAllTeachersAndStudents(groupName, courseName);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public CreateUserResponse createUser(@Valid @RequestBody UserCreate userCreate) {
        return userService.createUser(userCreate);
    }

    @DeleteMapping("/{userId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable UUID userId) {
        userService.deleteUser(userId);
    }
}
